# 15-puzzle

The 15-puzzle is a sliding puzzle that consists of a frame of numbered square tiles in random order with one tile missing. The object of the puzzle is to place the tiles in order by making sliding moves that use the empty space.

### Tech
15-puzzle uses a number of open source projects to work properly:

* [ReactJs] - is a JavaScript library for building user interfaces!
* [Babel] -  is a JavaScript compiler
* [Sass] (Syntactically awesome style sheets) - is a style sheet language
* [Webpack] -  is an open-source JavaScript module bundler

You can play in 15-puzzle at this link at [GitHub Pages][github-pages]

### Todos

 - Button "#shufle" can generate unsolvable position of squares.
 - Annimation of moving main square.
 - Feautiful victory message (modal window).

License
----
MIT

   [github-pages]: <(88maxwell.github.io)>
   [Babel]: <https://babeljs.io/>
   [ReactJs]: <https://reactjs.org/>
   [Webpack]: <https://webpack.js.org/>
   [Sass]: <http://sass-lang.com/>
